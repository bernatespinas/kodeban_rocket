use rocket::{Request, Response};
use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::{Header, Method, Status};

pub struct Cors;

#[rocket::async_trait]
impl Fairing for Cors {
	fn info(&self) -> Info {
		Info {
			name: "Add CORS headers to responses and mask missing OPTIONS endpoints",
			kind: Kind::Response
		}
	}

	async fn on_response<'r>(
		&self,
		request: &'r Request<'_>,
		response: &mut Response<'r>
	) {
		response.set_header(Header::new(
			"Access-Control-Allow-Origin",
			"*"
		));
		response.set_header(Header::new(
			"Access-Control-Allow-Methods",
			"POST, GET, PATCH, OPTIONS"
		));
		response.set_header(Header::new(
			"Access-Control-Allow-Headers",
			"*"
		));
		response.set_header(Header::new(
			"Access-Control-Allow-Credentials",
			"true"
		));

		if request.method() == Method::Options {
			println!("About to return Response to Request with Method::Options, forcing Status::Ok with CORS headers in place.");
			
			response.set_status(Status::Ok);
			response.set_sized_body(0, std::io::Cursor::new("inner"));
		}
	}
}