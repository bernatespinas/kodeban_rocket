use rocket::Route;
use rocket::serde::json::Json;

use crate::RocketPgPool;

#[derive(Debug, Serialize, Deserialize)]
struct Priority {
	id: i32,
	name: String,
}

pub fn routes() -> Vec<Route> {
	routes![
		get_all
	]
}

#[get("/")]
async fn get_all(pool: &RocketPgPool) -> Json<Vec<Priority>> {
	sqlx::query_as!(
		Priority,
		"SELECT * FROM priority ORDER BY id ASC"
	)
		.fetch_all(pool.inner())
		.await
		.ok()
		.map(Json::from)
		.unwrap()
}