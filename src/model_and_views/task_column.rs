use rocket::Route;
use rocket::serde::json::Json;

use crate::RocketPgPool;

use super::task::Task;

#[derive(Debug, Serialize, Deserialize)]
struct TaskColumn {
	id: i32,
	title: String,

	visible_in_board: bool,
}

pub fn routes() -> Vec<Route> {
	routes![
		get_all,
		get_by_id,
		get_tasks_by_task_column_id,
		get_visible_in_board,
		get_tasks_in_backlog,
		get_tasks_in_done
	]
}

#[get("/")]
async fn get_all(
	pool: &RocketPgPool
) -> Json<Vec<TaskColumn>> {
	sqlx::query_as!(
		TaskColumn,
		"SELECT * FROM task_column"
	)
		.fetch_all(pool.inner())
		.await
		.ok()
		.map(Json::from)
		.unwrap()
}

#[get("/board")]
async fn get_visible_in_board(
	pool: &RocketPgPool
) -> Json<Vec<TaskColumn>> {
	sqlx::query_as!(
		TaskColumn,
		"SELECT * FROM task_column WHERE visible_in_board"
	)
		.fetch_all(pool.inner())
		.await
		.ok()
		.map(Json::from)
		.unwrap()
}

#[get("/backlog/task")]
async fn get_tasks_in_backlog(
	pool: &RocketPgPool
) -> Json<Vec<Task>> {
	let backlog_task_column_id: i32 = sqlx::query!(
		"SELECT id FROM task_column WHERE title = 'Backlog'"
	)
		.fetch_one(pool.inner())
		.await
		.unwrap()
		.id
	;

	get_tasks_by_task_column_id(backlog_task_column_id, pool).await
}

#[get("/done/task")]
async fn get_tasks_in_done(
	pool: &RocketPgPool
) -> Json<Vec<Task>> {
	let done_task_column_id: i32 = sqlx::query!(
		"SELECT id FROM task_column WHERE title = 'Done'"
	)
		.fetch_one(pool.inner())
		.await
		.unwrap()
		.id
	;

	get_tasks_by_task_column_id(done_task_column_id, pool).await
}

#[get("/<id>")]
async fn get_by_id(
	id: i32,
	pool: &RocketPgPool
) -> Option<Json<TaskColumn>> {
	sqlx::query_as!(
		TaskColumn,
		"SELECT * FROM task_column WHERE id = $1",
		id
	)
		.fetch_one(pool.inner())
		.await
		.ok()
		.map(Json::from)
}

#[get("/<id>/task")]
async fn get_tasks_by_task_column_id(
	id: i32,
	pool: &RocketPgPool
) -> Json<Vec<Task>> {
	sqlx::query_as!(
		Task,
		"SELECT * FROM task WHERE task_column_id = $1 ORDER BY priority_id DESC",
		id
	)
		.fetch_all(pool.inner())
		.await
		.ok()
		.map(Json::from)
		.unwrap()
}