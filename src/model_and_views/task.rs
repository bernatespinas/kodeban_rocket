use rocket::Route;
use rocket::serde::json::Json;

use crate::RocketPgPool;

use super::json_response::JsonResponse;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Task {
	pub id: i32,
	pub title: String,

	pub task_column_id: i32,
	pub priority_id: i32,

	pub created_by_member_id: i32,
	pub assigned_to_member_id: Option<i32>,
}

pub fn routes() -> Vec<Route> {
	routes![
		get_by_id,
		delete_by_id,
		insert,
		patch
	]
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct InsertableTask {
	title: String,

	task_column_id: i32,
	priority_id: i32,

	created_by_member_id: i32,
	assigned_to_member_id: Option<i32>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct PatchableTask {
	title: Option<String>,

	task_column_id: Option<i32>,
	priority_id: Option<i32>,
	assigned_to_member_id: Option<i32>,
}

#[get("/<id>")]
async fn get_by_id(id: i32, pool: &RocketPgPool) -> Option<Json<Task>> {
	sqlx::query_as!(
		Task,
		"SELECT * FROM task WHERE id = $1",
		id
	)
		.fetch_one(pool.inner())
		.await
		.ok()
		.map(Json::from)
}

#[delete("/<id>")]
async fn delete_by_id(
	id: i32,
	pool: &RocketPgPool
) -> JsonResponse {
	sqlx::query!("DELETE FROM task WHERE id = $1", id)
		.execute(pool.inner())
		.await
		.unwrap()
	;

	JsonResponse::empty_ok()
}

#[post("/", data = "<insertable_task>")]
async fn insert(
	insertable_task: Json<InsertableTask>,
	pool: &RocketPgPool
) -> JsonResponse {
	if insertable_task.title.len() > 120 {
		println!("> 120");

		return JsonResponse::bad_request(
			"Title can have 120 characters at most"
		);
	}

	sqlx::query!(
		r#"INSERT INTO task (
			title,
			task_column_id,
			priority_id,
			created_by_member_id,
			assigned_to_member_id
		) VALUES ($1, $2, $3, $4, $5)"#,
		insertable_task.title,
		insertable_task.task_column_id,
		insertable_task.priority_id,
		insertable_task.created_by_member_id,
		insertable_task.assigned_to_member_id
	)
		.execute(pool.inner())
		.await
		.unwrap()
	;

	JsonResponse::empty_ok()
}

async fn update(
	patched_task: Json<Task>,
	pool: &RocketPgPool
) -> JsonResponse {
	let result = sqlx::query!(
		r#"UPDATE task
			SET
				title = $1,
				task_column_id = $2,
				priority_id = $3
			WHERE
				id = $4"#,
		patched_task.title,
		patched_task.task_column_id,
		patched_task.priority_id,
		patched_task.id
	)
		.execute(pool.inner())
		.await
	;

	match result {
		Ok(_) => JsonResponse::empty_ok(),
		Err(error) => JsonResponse::sqlx_error(error)
	}
}

#[patch("/<id>", data = "<patchable_task>")]
async fn patch(
	id: i32,
	patchable_task: Json<PatchableTask>,
	pool: &RocketPgPool
) -> JsonResponse {
	let mut patched_task = get_by_id(id, pool).await.unwrap();

	if let Some(title) = &patchable_task.title {
		patched_task.title = title.to_string();
	}

	if let Some(task_column_id) = patchable_task.task_column_id {
		patched_task.task_column_id = task_column_id;
	}

	if let Some(priority_id) = patchable_task.priority_id {
		patched_task.priority_id = priority_id;
	}

	update(patched_task, pool).await
}