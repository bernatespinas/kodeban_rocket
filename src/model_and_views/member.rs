use rocket::Route;
use rocket::serde::json::Json;

use crate::RocketPgPool;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct Member {
	id: i32,
	full_name: String,
	email: String,
}

pub fn routes() -> Vec<Route> {
	routes![
		get_all,
		get_by_id
	]
}

#[get("/")]
async fn get_all(
	pool: &RocketPgPool
) -> Json<Vec<Member>> {
	sqlx::query_as!(
		Member,
		"SELECT * FROM member"
	)
		.fetch_all(pool.inner())
		.await
		.ok()
		.map(Json::from)
		.unwrap()
}

#[get("/<id>")]
async fn get_by_id(
	id: i32,
	pool: &RocketPgPool
) -> Option<Json<Member>> {
	sqlx::query_as!(
		Member,
		"SELECT * FROM member WHERE id = $1",
		id
	)
		.fetch_one(pool.inner())
		.await
		.ok()
		.map(Json::from)
}