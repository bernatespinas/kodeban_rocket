use rocket::response::Responder;
use rocket::response::status::BadRequest;

#[derive(Responder, Debug)]
pub enum JsonResponse {
	#[response(status = 202, content_type = "json")]
	Ok(serde_json::Value),

	BadRequest(BadRequest<String>),
}

impl JsonResponse {
	// pub fn ok(value: serde_json::Value) -> JsonResponse {
	// 	JsonResponse::Ok(value)
	// }

	pub fn empty_ok() -> JsonResponse {
		JsonResponse::Ok(serde_json::json!({}))
	}

	pub fn bad_request<T: Into<String>>(value: T) -> JsonResponse {
		JsonResponse::BadRequest(
			BadRequest(Some(value.into()))
		)
	}

	pub fn sqlx_error(error: sqlx::Error) -> JsonResponse {
		let value = error.to_string();

		JsonResponse::bad_request(value)
	}
}