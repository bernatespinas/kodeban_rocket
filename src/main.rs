#[macro_use] extern crate serde;
#[macro_use] extern crate rocket;

use sqlx::{Pool, Postgres, postgres::PgPoolOptions};

mod cors;
pub use cors::Cors;

mod model_and_views;

type RocketPgPool = rocket::State<Pool<Postgres>>;
// type SqlxPgError = Result<sqlx::postgres::PgQueryResult, sqlx::Error>;

async fn create_pool() -> Result<Pool<Postgres>, sqlx::Error> {
	let database_url = dotenv::var("DATABASE_URL")
		.expect("Missing DATABASE_URL property in .env file")
	;

	PgPoolOptions::new()
		.max_connections(5)
		.connect(&database_url)
		.await
}

#[launch]
async fn rocket() -> _ {
	dotenv::dotenv().ok();

	let pool = create_pool().await
		.expect("Error creating database pool")
	;

	rocket::build()
		.attach(cors::Cors)
		.manage(pool)
		.mount("/task_column", model_and_views::task_column::routes())
		.mount("/task", model_and_views::task::routes())
		.mount("/member", model_and_views::member::routes())
		.mount("/priority", model_and_views::priority::routes())
}