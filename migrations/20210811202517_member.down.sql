BEGIN;

ALTER TABLE task DROP CONSTRAINT task_created_by_member_id_fkey;
ALTER TABLE task DROP COLUMN created_by_member_id;

ALTER TABLE task DROP CONSTRAINT task_assigned_to_member_id_fkey;
ALTER TABLE task DROP COLUMN assigned_to_member_id;

DROP TABLE member;

DROP TRIGGER check_task_update ON task;
DROP FUNCTION check_task_update;

COMMIT;