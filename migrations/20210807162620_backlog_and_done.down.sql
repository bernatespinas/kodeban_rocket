DELETE FROM task;
ALTER SEQUENCE task_id_seq RESTART;

DELETE FROM task_column;
ALTER SEQUENCE task_column_id_seq RESTART;

ALTER TABLE task_column DROP COLUMN visible_in_board;

INSERT INTO task_column (title) VALUES
	('To do'),
	('In progress'),
	('Ready to test'),
	('Testing')
;