-- Table priority.

CREATE TABLE priority (
	id SERIAL,
	name VARCHAR(20) NOT NULL,

	PRIMARY KEY (id)
);

INSERT INTO priority (name) VALUES ('Low'), ('Medium'), ('High'), ('Urgent');

-- TABLE task: add priority_id.

ALTER TABLE task ADD COLUMN priority_id INTEGER;

ALTER TABLE task ADD CONSTRAINT task_priority_id_fkey
	FOREIGN KEY (priority_id) REFERENCES priority (id)
;

UPDATE task SET priority_id = (SELECT id FROM priority WHERE name = 'Low');

ALTER TABLE task ALTER COLUMN priority_id SET NOT NULL;