DELETE FROM task;
ALTER SEQUENCE task_id_seq RESTART;
-- UPDATE task SET id = DEFAULT;

DELETE FROM task_column;
ALTER SEQUENCE task_column_id_seq RESTART;
-- UPDATE task_column SET id = DEFAULT;

ALTER TABLE task_column ADD UNIQUE (title);

ALTER TABLE task_column ADD COLUMN visible_in_board BOOLEAN;

INSERT INTO task_column (title, visible_in_board) VALUES
	('Backlog', false),
	('To do', true),
	('In progress', true),
	('Ready to test', true),
	('Testing', true),
	('Done', false)
;

ALTER TABLE task_column ALTER COLUMN visible_in_board SET NOT NULL;
