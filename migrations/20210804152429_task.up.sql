-- task_column

CREATE TABLE task_column(
	id SERIAL,
	title VARCHAR NOT NULL,

	PRIMARY KEY (id)
);

INSERT INTO task_column (title) VALUES ('To do');
INSERT INTO task_column (title) VALUES ('In progress');
INSERT INTO task_column (title) VALUES ('Ready to test');
INSERT INTO task_column (title) VALUES ('Testing');

-- task

CREATE TABLE task (
	id SERIAL,
	title VARCHAR(120) NOT NULL,

	task_column_id INTEGER NOT NULL,

	PRIMARY KEY (id),
	FOREIGN KEY (task_column_id) REFERENCES task_column (id)
);