ALTER TABLE task DROP CONSTRAINT task_priority_id_fkey;
ALTER TABLE task DROP COLUMN priority_id;

DROP TABLE priority;