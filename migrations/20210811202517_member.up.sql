BEGIN;

CREATE TABLE member (
	id SERIAL,
	full_name VARCHAR(50) NOT NULL,
	email VARCHAR(50) UNIQUE NOT NULL,

	PRIMARY KEY (id)
);

-- Create placeholder member.
INSERT INTO member (id, full_name, email) VALUES (
	1,
	'Bernat Espinàs Añor',
	'bernatespinas@gmail.com'
);

-- task: create and update created_by_member_id

ALTER TABLE task ADD COLUMN created_by_member_id INTEGER;
ALTER TABLE task ADD CONSTRAINT task_created_by_member_id_fkey
	FOREIGN KEY (created_by_member_id) REFERENCES member (id)
;

UPDATE task SET created_by_member_id = 1;

ALTER TABLE task ALTER COLUMN created_by_member_id SET NOT NULL;

-- task: create assigned_to_member_id

ALTER TABLE task ADD COLUMN assigned_to_member_id INTEGER;
ALTER TABLE task ADD CONSTRAINT task_assigned_to_member_id_fkey
	FOREIGN KEY (assigned_to_member_id) REFERENCES member (id)
;

-- Trigger to ___.
CREATE FUNCTION check_task_update() RETURNS TRIGGER AS $$
BEGIN

	IF NEW.task_column_id IN (2, 3, 4, 5, 6) AND NEW.assigned_to_member_id IS NULL THEN
		RAISE EXCEPTION 'Tasks which are not in Backlog must have an assigned member';
	END IF;

	RETURN NEW;

END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER check_task_update
	BEFORE UPDATE OR INSERT ON task
	FOR EACH ROW
	EXECUTE FUNCTION check_task_update()
;

COMMIT;